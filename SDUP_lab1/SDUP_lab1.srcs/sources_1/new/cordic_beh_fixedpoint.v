module cordic_beh_fixedpoint();
/**
* Cordic algorithm
*/
//real t_angle = 0.8; //Input parameter. The angle
//Table of arctan (1/2^i)
// Note. Table initialization below is not correct for Verilog. Select System-Verilog mode
// in your simulator in the case of syntax errors

parameter integer FXP_SCALE  = 1024;
parameter real R_FXP_SCALE = 1024.0;

reg signed [11:0] t_angle = 0.8 * FXP_SCALE;
reg signed [11:0] cos = 1.0 * FXP_SCALE;
reg signed [11:0] sin = 0.0 * FXP_SCALE;
reg signed [11:0] angle = 0.0 * FXP_SCALE;
reg signed [11:0] arctan[0:10] = { 0.785398163* FXP_SCALE, 0.463647609* FXP_SCALE, 0.244978663* FXP_SCALE, 0.124354995* FXP_SCALE, 0.06241881* FXP_SCALE,
 0.031239833* FXP_SCALE, 0.015623729* FXP_SCALE, 0.007812341* FXP_SCALE, 0.00390623* FXP_SCALE, 0.001953123* FXP_SCALE,
 0.000976562* FXP_SCALE}; 
 
 reg signed [11:0] Kn = 0.607253 * FXP_SCALE;

 
//real arctan[0:10] = { 0.785398163, 0.463647609, 0.244978663, 0.124354995, 0.06241881,
// 0.031239833, 0.015623729, 0.007812341, 0.00390623, 0.001953123,
// 0.000976562 };
//real Kn = 0.607253; //Cordic scaling factor for 10 iterations
////Variables
//real cos = 1.0; //Initial vector x coordinate
//real sin = 0.0; //and y coordinate
//real angle = 0.0; //A running angle

integer i, d, j;
reg signed [11:0] tmp;
reg signed [23:0] sin_o;
reg signed [23:0] cos_o;

initial //Execute only once
begin
     for ( i = 0; i < 10; i = i + 1) //Ten algorithm iterations
     begin
         if( t_angle > angle )
         begin
             angle = angle + arctan[i];
            
             tmp = cos - ( sin >>> i );
             sin = ( cos >>> i ) + sin;
             cos = tmp;
         end
         else
         begin
             angle = angle - arctan[i];
             tmp = cos + ( sin >>> i );
             sin = - ( cos >>> i) + sin;
             cos = tmp;
         end //if
     end //for
     //Scale sin/cos values
    sin_o = (Kn * sin) >>> 10; // shift right to keep binary point consistent 
    cos_o = (Kn * cos) >>> 10;
      
     $display("sin=%f, cos=%f", r_sin_o, r_cos_o);
end

endmodule;