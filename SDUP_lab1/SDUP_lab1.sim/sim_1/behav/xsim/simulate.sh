#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2018.3 (64-bit)
#
# Filename    : simulate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for simulating the design by launching the simulator
#
# Generated by Vivado on Sun Mar 14 22:44:44 CET 2021
# SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
#
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
#
# usage: simulate.sh
#
# ****************************************************************************
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep xsim cordic_rtl_TB_behav -key {Behavioral:sim_1:Functional:cordic_rtl_TB} -tclbatch cordic_rtl_TB.tcl -view /home/wonkie/Rozne/AGieHa/Magisterskie/SDUP/SDUP_repo/SDUP_lab1/cordic_beh_fixedpoint_behav.wcfg -log simulate.log
