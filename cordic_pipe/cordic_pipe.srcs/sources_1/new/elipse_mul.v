`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/25/2021 09:57:44 PM
// Design Name: 
// Module Name: elipse_mul
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module elipse_mul(clock, ce, value_in, value_out);
parameter integer W = 14; //Width of the fixed-point (12:10) representation
parameter FXP_SHIFT = 10; //Fraction for fixed-point (12:10) representation
parameter integer MUL = 1;
input clock, ce;
input signed[W-1:0] value_in;
output reg signed[W-1:0] value_out;
reg signed [2*W-1:0] val, val_shifted_0, val_shifted_1; //Shifted input values
reg signed [2*W-1:0] val_mul; //Accumulated values
//
always @ (posedge clock)
begin
if( ce == 1'b1 )
 begin
     //Step S4
     val = value_in; 
     if( MUL == 3)
     begin
        val_shifted_0 <= val << 10;
        val_shifted_1 <= val << 12;
     end
     else if( MUL == 7)
     begin
        val_shifted_0 <= val << 10;
        val_shifted_1 <= val << 13;
     end
     //Step S5
     val_mul <= val_shifted_1 - val_shifted_0;
     //Step S6
     value_out <= val_mul >>> FXP_SHIFT;
 end
end
endmodule
