`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/25/2021 04:39:49 PM
// Design Name: 
// Module Name: cordic_pipe_rtl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////
// Design Name: The pipelined custom processor for cordic algorithm
// Module Name: cordic_pipe_rtl
//////////////////////////////////////////////////////////////////////////////////

module elipse_cordic_pipe_beh( clock, reset, ce, angle_in, x_out, y_out, valid_out);
parameter reg[W-1:0] a, b;
parameter integer W = 12; //Width of the fixed-point (12:10) representation
parameter FXP_MUL = 1024; //Scaling factor for fixed-point (12:10) representation
parameter PIPE_LATENCY = 17; // Input->output delay in clock cycles
input clock, reset, ce;
input [W-1:0] angle_in; //Angle in radians
output reg [W-1:0] x_out, y_out;
output valid_out; //Valid data output flag
wire [W-1:0] sin_out, cos_out;
reg signed [2*W-1:0]  x_temp, y_temp;
//Cordic look-up table
reg signed [W-1:0] atan[0:10] = {  'b001100100100, 'b000111011011, 'b000011111011, 'b000001111111,
                                  'b000001000000, 'b000000100000, 'b000000010000, 'b000000001000,   
                                  'b000000000100, 'b000000000010, 'b000000000001 }; 
//Tabs of wires for connections between the stage processors a2 - a13
wire signed [W-1:0] sin_tab [0:11];
wire signed [W-1:0] cos_tab [0:11];
wire signed [W-1:0] t_angle_tab [0:11]; //Target angle also must be pipelined
wire signed [W-1:0] angle_tab [0:11];
//
reg unsigned [4:0] valid_cnt; //Counts pipeline delay
//Synchroniuos activity: latency counter, angle_in latch
always@(posedge clock)
begin
 if ( reset == 1'b1 )
 valid_cnt <= PIPE_LATENCY; //Setup latency counter
 else
 if( ( valid_cnt != 0 ) && ( ce == 1'b1 ) )
 valid_cnt <= valid_cnt - 1; //Valid output data moves toward output
 
 if ( reset == 1'b0 && ce == 1'b1 )
 begin
    x_temp <= cos_out * (a*FXP_MUL);
    x_out <= x_temp / FXP_MUL;
    
    y_temp <= sin_out * (b*FXP_MUL);
    y_out <= y_temp / FXP_MUL;
 end
end

assign valid_out = ( valid_cnt == 0 )? 1'b1 : 1'b0; //Set valid_out when counter counts up to PIPE_LATENCY
//Stage a1: assign initial values (No registers - asynchronous !!!)
assign cos_tab[0] = 1.0 * FXP_MUL;
assign sin_tab[0] = 0;
assign angle_tab[0] = 0;
assign t_angle_tab[0] = angle_in;
//Stage a2 - 13 processor netlist

generate for (genvar j=0; j<11; j=j+1)
    begin: cordic_step_loop
    cordic_step #(.step(j), .W(W)) cordic_step_j (clock, ce, sin_tab[j], cos_tab[j], angle_tab[j], t_angle_tab[j], atan[j],
 sin_tab[j+1], cos_tab[j+1], angle_tab[j+1], t_angle_tab[j+1]);
    end //end of the for loop inside the generate block
endgenerate //end of the generate block

//Stage a14 - 18: scaling of the results
 mul_Kn #(.W(W)) mul_Kn_sin ( clock, ce, sin_tab[11], sin_out );
 mul_Kn #(.W(W)) mul_Kn_cos ( clock, ce, cos_tab[11], cos_out );
 
 //elipse_mul #(.MUL(a)) mul_x(clock, ce, cos_out, x_out);
 //elipse_mul #(.MUL(b)) mul_y(clock, ce, sin_out, y_out);
endmodule
