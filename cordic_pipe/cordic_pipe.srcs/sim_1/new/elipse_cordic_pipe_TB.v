`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/25/2021 04:44:40 PM
// Design Name: 
// Module Name: cordic_pipe_rtl_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module elipse_cordic_pipe_TB;
parameter integer W = 15;
reg clock, ce, reset, start;
reg [W-1:0] angle_in;
real angle;
wire [W-1:0] x_out, y_out;
wire valid_out;
//For easy output value monitoring
real real_x, real_y;
//Instantiation
//elipse_cordic_pipe_beh #(.a(3), .b(7), .W(W)) cordic ( clock, reset, ce, angle_in, x_out, y_out, valid_out );
elipse_cordic_pipe_rtl #(.a(3), .b(7), .W(W)) cordic ( clock, reset, ce, angle_in, x_out, y_out, valid_out );
//Reset stimuli
initial
begin
 reset <= 1'b1;
 #10 reset <= 1'b0;
end
//ce & clock generator stimuli
initial
begin
 ce <= 1'b1;
 clock <= 1'b1;
end
always
 #5 clock <= ~clock;
//Signals stimuli
initial
angle = 0.0;
always@(posedge clock)
begin
 if (angle < 3.14/2) angle = angle + 0.1; else angle = 0;
 angle_in <= angle * 1024; //Value in fixed-point (12:10)
 //Convert and display results
 real_x = x_out;
 real_y = y_out;
 real_x = real_x / 1024;
 real_y = real_y / 1024;
 $display("Real values: x=%f, y=%f", real_x, real_y);
end
endmodule
